package com.jorgebeltrannunez.restaurantkeepcoding.util

import android.content.Context
import android.support.v7.widget.LinearLayoutCompat
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Allergen
import java.util.*

fun ClosedRange<Int>.random() =
        Random().nextInt(endInclusive - start) + start

fun generateLinearGridLayoutAllergens(context: Context, linearLayoutParent: LinearLayout, columns: Int, allergens: MutableList<Allergen>) {
    var allergen = 0
    while (allergen < allergens.size) {
        val linearLayout = LinearLayout(context)
        linearLayoutParent.addView(linearLayout)
        linearLayout.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        linearLayout.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        linearLayout.orientation = LinearLayout.HORIZONTAL
        var column = 0
        while (column < columns) {
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1F)
            if (allergen < allergens.size) {
                val imageView = ImageView(context)
                imageView.setImageResource(getImageAllergen(allergens[allergen].id))
                linearLayout.addView(imageView)
//                imageView.maxWidth = 25
//                imageView.layoutParams.width = 50
                imageView.layoutParams = params
                val padding = 10
                imageView.setPadding(padding,padding,padding,padding)
                imageView.adjustViewBounds = true
            } else {
                val viewEmpty = View(context)
                viewEmpty.layoutParams =  params
                linearLayout.addView(viewEmpty)
            }
            column++
            allergen++
        }
    }
}