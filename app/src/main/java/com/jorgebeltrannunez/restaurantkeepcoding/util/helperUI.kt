package com.jorgebeltrannunez.restaurantkeepcoding.util

import com.jorgebeltrannunez.restaurantkeepcoding.R

fun getImageDish(id: Int): Int =
        when (id) {
            0 -> R.drawable.food_0
            1 -> R.drawable.food_1
            2 -> R.drawable.food_2
            3 -> R.drawable.food_3
            4 -> R.drawable.food_4
            5 -> R.drawable.food_5
            6 -> R.drawable.food_6
            7 -> R.drawable.food_7
            8 -> R.drawable.food_8
            9 -> R.drawable.food_9
            else -> R.drawable.food_0
        }


fun getImageAllergen(id: Int): Int =
        when (id) {
            0 -> R.drawable.allergen_0
            1 -> R.drawable.allergen_1
            2 -> R.drawable.allergen_2
            3 -> R.drawable.allergen_3
            4 -> R.drawable.allergen_4
            5 -> R.drawable.allergen_5
            6 -> R.drawable.allergen_6
            7 -> R.drawable.allergen_7
            8 -> R.drawable.allergen_8
            9 -> R.drawable.allergen_9
            else -> R.drawable.allergen_0
        }