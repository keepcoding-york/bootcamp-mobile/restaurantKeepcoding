package com.jorgebeltrannunez.restaurantkeepcoding

import android.app.Application
import com.jorgebeltrannunez.restaurantkeepcoding.repository.RestaurantRepository
import com.jorgebeltrannunez.restaurantkeepcoding.repository.api.RestaurantApi
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.TableListViewModel
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.BillListViewModel
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.DishListViewModel

class App : Application() {

    companion object {
        private lateinit var restaurantApi: RestaurantApi
        private lateinit var restaurantRepository: RestaurantRepository
        private lateinit var tableListViewModel: TableListViewModel
        private lateinit var billListViewModel: BillListViewModel
        private lateinit var dishListViewModel: DishListViewModel

        fun injectRestaurantApi() = restaurantApi
        fun injectRestaurantRepository() = restaurantRepository

        fun injectTableListViewModel() = tableListViewModel
//        fun injectBillListViewModel() = billListViewModel
        fun injectBillListViewModel() = BillListViewModel(restaurantRepository)
//        fun injectDishListViewModel() = dishListViewModel
        fun injectDishListViewModel() = DishListViewModel(restaurantRepository)
    }

    override fun onCreate() {
        super.onCreate()
        restaurantApi = RestaurantApi()
        restaurantRepository = RestaurantRepository(restaurantApi)
        tableListViewModel = TableListViewModel(restaurantRepository)
//        billListViewModel = BillListViewModel(restaurantRepository)
//        dishListViewModel = DishListViewModel(restaurantRepository)
    }
}
