package com.jorgebeltrannunez.restaurantkeepcoding.repository.model

data class Dish (val id: Int, val name: String, val description: String, var price: Int, var allergens: MutableList<Allergen>, var note: String? = null)