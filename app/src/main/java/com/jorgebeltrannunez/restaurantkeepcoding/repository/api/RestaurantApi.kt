package com.jorgebeltrannunez.restaurantkeepcoding.repository.api

import android.util.Log
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Allergen
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Bill
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Dish
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Table
import com.jorgebeltrannunez.restaurantkeepcoding.util.random
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class RestaurantApi {
    private val numberTables = 20
    private val numberDishes = 10
    private val numberAllergen = 10
    private val delay: Long = 0

    private var tables: MutableList<Table> = mutableListOf()

    init {
        for (i in 0..numberTables) {
            tables.add(Table(i))
        }
    }

    private var dishes: MutableList<Dish> = mutableListOf()

    init {
        for (i in 0..numberDishes) {
            val numberAllergensInDish = (0..numberAllergen).random()
            val allergensInDish = ArrayList<Allergen>()
            for (allergenId in 0..numberAllergensInDish) {
                allergensInDish.add(Allergen(allergenId, "Allergen $i"))
            }
            dishes.add(Dish(i, "Dish $i", "Description of dish", i * 2, allergensInDish))
        }
    }

    private var bills: MutableList<Bill> = mutableListOf()

    init {
//        for (i in 0..numberTables) {
//            for (x in 0..20) {
//                bills.add(Bill(bills.size + 1, dishes, i, false, dishes.map { it.price }.reduce { acc, i -> acc + i }))
//            }
//        }
    }


    fun getTables(): Observable<MutableList<Table>> {
        return Observable.just(tables).delay(delay, TimeUnit.SECONDS)
    }

    fun getTable(id: Int): Observable<Table> {
        val table = tables.first { table -> table.id == id }
        return Observable.just(table).delay(delay, TimeUnit.SECONDS)
    }

    fun getBills(tableId: Int): Observable<MutableList<Bill>> {
        val bills = bills.filter { bill -> bill.tableId == tableId }.toMutableList()
        return Observable.just(bills).delay(delay, TimeUnit.SECONDS)
    }

    fun getDishes(): Observable<MutableList<Dish>> {
        return Observable.just(dishes).delay(delay, TimeUnit.SECONDS)
    }

    fun getDish(dishId: Int): Observable<Dish> {
        val dish = dishes.first { dish -> dish.id == dishId }
        return Observable.just(dish).delay(delay, TimeUnit.SECONDS)
    }

    fun getDishes(billId: Int): Observable<MutableList<Dish>> {
        var dishes = bills.firstOrNull { bill -> bill.id == billId }?.dishes
        if (dishes == null) {
            dishes = mutableListOf()
        }
        return Observable.just(dishes).delay(delay, TimeUnit.SECONDS)
    }

    fun addDishToBill(dishId: Int, billId: Int, note: String) {
        val bill = bills.first { bill -> bill.id == billId }
        val dish = dishes.first { dish -> dish.id == dishId }
        dish.note = note
        bill.dishes.add(dish)
        bill.total += dish.price
        Log.d("RepositoryApi", "addDishToBill()")
    }

    private fun getBill(billId: Int): Observable<Bill> {
        val bill = bills.first { bill -> bill.id == billId }
        return Observable.just(bill).delay(delay, TimeUnit.SECONDS)
    }

    fun addBillToTable(tableId: Int) {
        val bill = Bill(bills.size+1, mutableListOf(), tableId, false, 0)
        bills.add(bill)
    }
}