package com.jorgebeltrannunez.restaurantkeepcoding.repository.model

import android.media.Image

data class Allergen (val id: Int, val name: String)
