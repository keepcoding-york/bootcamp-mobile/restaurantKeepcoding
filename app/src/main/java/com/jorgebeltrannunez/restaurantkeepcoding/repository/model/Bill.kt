package com.jorgebeltrannunez.restaurantkeepcoding.repository.model

data class Bill (val id: Int, var dishes: MutableList<Dish>, val tableId: Int, var paid: Boolean, var total: Int)