package com.jorgebeltrannunez.restaurantkeepcoding.repository

import android.util.Log
import com.jorgebeltrannunez.restaurantkeepcoding.repository.api.RestaurantApi
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Bill
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Dish
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Table
import io.reactivex.Observable

class RestaurantRepository(val restaurantApi: RestaurantApi) {

    fun getTables(): Observable<MutableList<Table>> {
        return restaurantApi.getTables()
                .doOnNext {
                    Log.d("RestaurantRepository", "getTables()")
                }
    }
    fun getDishes(): Observable<MutableList<Dish>> {
        return restaurantApi.getDishes()
                .doOnNext {
                    Log.d("RestaurantRepository", "getDishes()")
                }
    }
    fun getDishes(billId: Int): Observable<MutableList<Dish>> {
        return restaurantApi.getDishes(billId)
                .doOnNext {
                    Log.d("RestaurantRepository", "getDishes($billId)")
                }
    }
    fun getBills(tableId: Int): Observable<MutableList<Bill>> {
        return restaurantApi.getBills(tableId)
                .doOnNext {
            Log.d("RestaurantRepository", "getDishes()")
        }
    }

    fun addDishToBill(dishId: Int, billId: Int, note: String) {
        restaurantApi.addDishToBill(dishId, billId, note)
    }

    fun addBillToTable(tableId: Int) {
        restaurantApi.addBillToTable(tableId)
    }
}