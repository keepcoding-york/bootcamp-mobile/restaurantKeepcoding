package com.jorgebeltrannunez.restaurantkeepcoding.viewmodel

import android.util.Log
import com.jorgebeltrannunez.restaurantkeepcoding.repository.RestaurantRepository
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.model.TablesList
import io.reactivex.Observable


class TableListViewModel(val restaurantRepository: RestaurantRepository) {

    fun getTables(): Observable<TablesList> {
        return restaurantRepository.getTables()
                .map {
                    Log.d("TableListViewModel", "Mapping users to UIData...")
                    TablesList(it, "Get tables")
                }
                .onErrorReturn {
                    TablesList(emptyList(), "An error occurred", it)
                }
    }

}