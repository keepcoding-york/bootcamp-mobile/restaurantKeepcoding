package com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.model

import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Table
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Bill
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Dish

data class TablesList(val tables: List<Table>, val message: String, val error: Throwable? = null)
data class BillsList(val bills: List<Bill>, val message: String, val error: Throwable? = null)
data class DishesList(val dishes: List<Dish>, val message: String, val error: Throwable? = null)
