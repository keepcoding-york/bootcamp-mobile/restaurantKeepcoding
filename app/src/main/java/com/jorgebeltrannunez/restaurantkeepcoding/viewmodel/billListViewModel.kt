package com.jorgebeltrannunez.restaurantkeepcoding.viewmodel

import android.util.Log
import com.jorgebeltrannunez.restaurantkeepcoding.repository.RestaurantRepository
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.model.BillsList
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.model.TablesList
import io.reactivex.Observable

class BillListViewModel(val restaurantRepository: RestaurantRepository) {
    // TODO Inicializar tableId en el constructor cuando inyecto el view model o a posterior como actualmente
    var tableId: Int = 0

    fun getBills(): Observable<BillsList> {
        return restaurantRepository.getBills(tableId)
                .map {
                    Log.d("TableListViewModel", "get Bills")
                    BillsList(it, "BillListViewModel -> getBills()")
                }
                .onErrorReturn {
                    BillsList(emptyList(), "An error occurred", it)
                }
    }
}