package com.jorgebeltrannunez.restaurantkeepcoding.viewmodel

import com.jorgebeltrannunez.restaurantkeepcoding.App
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.ListRecycleView
import io.reactivex.Observable

class BillViewModel(val billId: Int) {
    val repository = App.injectRestaurantRepository()
    lateinit var dishesList: ListRecycleView
}
