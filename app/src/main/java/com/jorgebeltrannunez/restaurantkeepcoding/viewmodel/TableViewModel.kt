package com.jorgebeltrannunez.restaurantkeepcoding.viewmodel

import com.jorgebeltrannunez.restaurantkeepcoding.App
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.ListRecycleView
import io.reactivex.Observable

class TableViewModel(val tableId: Int) {
    val repository =  App.injectRestaurantRepository()
    lateinit var billsList: ListRecycleView
    fun addBill(): Observable<String> {
        repository.addBillToTable(tableId)
        billsList.refreshList()
        return Observable.just("Bill Created")
    }
}