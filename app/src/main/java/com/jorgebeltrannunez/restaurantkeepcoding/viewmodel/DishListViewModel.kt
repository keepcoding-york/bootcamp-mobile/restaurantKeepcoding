package com.jorgebeltrannunez.restaurantkeepcoding.viewmodel

import android.util.Log
import com.jorgebeltrannunez.restaurantkeepcoding.repository.RestaurantRepository
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.model.DishesList
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.model.TablesList
import io.reactivex.Observable

class DishListViewModel(val restaurantRepository: RestaurantRepository) {
    var billId: Int? = null

    fun getDishes(): Observable<DishesList> {
        val billId = billId
        return if (billId != null) {
            getDishes(billId)

        } else {
            restaurantRepository.getDishes()
                    .map {
                        Log.d("TableListViewModel", "Mapping dishes to UIData...")
                        DishesList(it, "Get dishes")
                    }
                    .onErrorReturn {
                        DishesList(emptyList(), "An error occurred", it)
                    }
        }
    }

    fun getDishes(billId: Int): Observable<DishesList> {
        return restaurantRepository.getDishes(billId)
                .map {
                    Log.d("TableListViewModel", "Mapping dishes to UIData...")
                    DishesList(it, "Get dishes")
                }
                .onErrorReturn {
                    DishesList(emptyList(), "An error occurred", it)
                }
    }

}