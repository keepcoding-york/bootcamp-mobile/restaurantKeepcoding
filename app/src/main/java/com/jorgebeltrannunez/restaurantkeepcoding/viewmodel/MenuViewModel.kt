package com.jorgebeltrannunez.restaurantkeepcoding.viewmodel

import com.jorgebeltrannunez.restaurantkeepcoding.App
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.ListRecycleView
import io.reactivex.Observable

class MenuViewModel(val billId: Int) {
    val repository = App.injectRestaurantRepository()
    lateinit var dishesList: ListRecycleView
    fun addDish(dishId: Int, note: String): Observable<String> {
        repository.addDishToBill(dishId, billId, note)
        dishesList.refreshList()
        return Observable.just("Dish added")
    }
}