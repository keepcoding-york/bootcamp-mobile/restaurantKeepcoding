package com.jorgebeltrannunez.restaurantkeepcoding.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Bill
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.BillListFragment
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.TableViewModel
import kotlinx.android.synthetic.main.activity_layout_sample.*

class TableActivity : AppCompatActivity(), BillListFragment.OnBillListFragmentInteractionListener {

    private lateinit var viewModel: TableViewModel
    private val tableId by lazy {
        intent.getIntExtra(ARG_TABLE_ID, 0)
    }

    override fun onBillListFragmentInteraction(billSelected: Bill?) {
        if (billSelected != null) {
            startActivity(BillActivity.intent(this , billSelected.id))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_table)
        toolbar_table.title = getString(R.string.table) + " " + tableId
        setSupportActionBar(toolbar_table)
        viewModel = TableViewModel(tableId)
        setupBillsList()
        setupFab()
    }

    private fun setupFab() {
        fab_table.setOnClickListener { view ->
            viewModel.addBill().subscribe({
                showSnackbar(it, view)
            })
        }
    }

    private fun setupBillsList() {
        val billsList = BillListFragment.newInstance(resources.getInteger(R.integer.bills_list_column), tableId)
        viewModel.billsList = billsList
        supportFragmentManager.beginTransaction()
                .replace(R.id.bill_list_table, billsList)
                .commit()
    }

    private fun showSnackbar(text: String, view: View) {
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show()
    }

    companion object {

        const val ARG_TABLE_ID = "ARG_TABLE_ID"

        fun intent(context: Context, tableId: Int): Intent {
            val intent = Intent(context, TableActivity::class.java)
            intent.putExtra(ARG_TABLE_ID, tableId)

            return intent
        }
    }

}
