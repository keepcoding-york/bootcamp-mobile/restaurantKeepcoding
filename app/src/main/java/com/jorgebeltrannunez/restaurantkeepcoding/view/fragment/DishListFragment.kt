package com.jorgebeltrannunez.restaurantkeepcoding.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jorgebeltrannunez.restaurantkeepcoding.App
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Dish
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.adapter.DishRecyclerViewAdapter

import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.util.MvvmFragment
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.model.DishesList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [DishListFragment.OnDishListFragmentInteractionListener] interface.
 */
class DishListFragment : MvvmFragment(), ListRecycleView {

    private var columnCount = 1
    private var billId: Int? = null
    private val dishListViewModel = App.injectDishListViewModel()
    private lateinit var recyclerView: RecyclerView
    private var listenerDish: OnDishListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupArguments()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_dish_list, container, false)
        setupRecyclerView(view)
        return view
    }

    override fun onStart() {
        super.onStart()
       refreshList()
    }

    override fun onResume() {
        super.onResume()
        refreshList()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnDishListFragmentInteractionListener) {
            listenerDish = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnDishListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listenerDish = null
    }

    override fun setupRecyclerView(view: View) {
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                recyclerView =  this
                adapter = DishRecyclerViewAdapter(emptyList(), listenerDish)
            }
        }
    }

    override fun setupArguments() {
        var showAllDishes: Boolean
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
            showAllDishes = it.getBoolean(ARG_SHOW_ALL_DISHES, false)
            billId = if (showAllDishes) {
                null
            } else {
                it.getInt(ARG_BILL_ID)
            }
        }
        dishListViewModel.billId = billId
    }

    override fun refreshList() {
        dishListViewModel.getDishes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d("onStart", "Received UIModel with dishes")
                    showDishes(it)
                }, {
                    showError()
                })
    }

    private fun showDishes(dishesList: DishesList) {
        if (!dishesList.dishes.isEmpty()) {
            recyclerView.adapter = DishRecyclerViewAdapter(dishesList.dishes, listenerDish)
        }
    }

    private fun showError() {
        Toast.makeText(context, getString(R.string.error_occurred), Toast.LENGTH_SHORT).show()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnDishListFragmentInteractionListener {
        fun onDishListFragmentInteraction(dish: Dish?)
    }

    companion object {

        const val ARG_COLUMN_COUNT = "column-count"
        const val ARG_BILL_ID = "ARG_BILL_ID"
        const val ARG_SHOW_ALL_DISHES = "ARG_SHOW_ALL_DISHES"

        @JvmStatic
        fun newInstance(columnCount: Int, showAllDishes: Boolean, billId: Int? = null) =
                DishListFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                        putBoolean(ARG_SHOW_ALL_DISHES, showAllDishes)
                        if (billId != null) {
                            putInt(ARG_BILL_ID, billId)
                        }
                    }
                }
    }
}
