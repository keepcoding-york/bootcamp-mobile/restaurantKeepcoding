package com.jorgebeltrannunez.restaurantkeepcoding.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Dish
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.DishListFragment
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.BillViewModel

import kotlinx.android.synthetic.main.activity_bill.*

class BillActivity : AppCompatActivity(), DishListFragment.OnDishListFragmentInteractionListener {

    private lateinit var viewModel: BillViewModel
    private val billId by lazy {
        intent.getIntExtra(ARG_BILL_ID, 0)
    }

    override fun onDishListFragmentInteraction(dish: Dish?) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bill)
        toolbar_bill.title = getString(R.string.bill) + " " + billId
        setSupportActionBar(toolbar_bill)
        viewModel = BillViewModel(billId)
        setupBillsList()
        setupFab()
    }

    private fun setupFab() {
        fab_bill.setOnClickListener { view ->
            startActivity(MenuActivity.intent(this ,billId))
        }
    }

    private fun setupBillsList() {
        val dishesList = DishListFragment.newInstance(resources.getInteger(R.integer.dishes_list_column), false, billId)
        viewModel.dishesList = dishesList
        supportFragmentManager.beginTransaction()
                .replace(R.id.dishes_list_bill, dishesList)
                .commit()
    }

    private fun showSnackbar(text: String, view: View) {
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show()
    }

    companion object {

        const val ARG_BILL_ID = "ARG_BILL_ID"

        fun intent(context: Context, tableId: Int): Intent {
            val intent = Intent(context, BillActivity::class.java)
            intent.putExtra(ARG_BILL_ID, tableId)

            return intent
        }
    }

}
