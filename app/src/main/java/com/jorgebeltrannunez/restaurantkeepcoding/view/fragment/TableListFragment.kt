package com.jorgebeltrannunez.restaurantkeepcoding.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jorgebeltrannunez.restaurantkeepcoding.App
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Table
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.adapter.TableRecyclerViewAdapter
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.util.MvvmFragment

import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.model.TablesList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [TableListFragment.OnTableListFragmentInteractionListener] interface.
 */
class TableListFragment : MvvmFragment(), ListRecycleView {

    private var columnCount = 1
    private val tableListViewModel = App.injectTableListViewModel()
    private lateinit var recyclerView: RecyclerView
    private var listenerTable: OnTableListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupArguments()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_table_list, container, false)
        setupRecyclerView(view)
        return view
    }

    override fun onStart() {
        super.onStart()
        refreshList()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnTableListFragmentInteractionListener) {
            listenerTable = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnTableListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listenerTable = null
    }

    override fun setupRecyclerView(view: View) {
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                recyclerView = this
                adapter = TableRecyclerViewAdapter(emptyList(), listenerTable)
            }
        }
    }

    override fun setupArguments() {
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun refreshList() {
        subscribe(tableListViewModel.getTables()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d("onStart", "Received UIModel with tables")
                    showTables(it)
                }, {
                    showError()
                }))
    }

    private fun showTables(tablesList: TablesList) {
        if (!tablesList.tables.isEmpty()) {
            recyclerView.adapter = TableRecyclerViewAdapter(tablesList.tables, listenerTable)
        }
    }

    private fun showError() {
        Toast.makeText(context, getString(R.string.error_occurred), Toast.LENGTH_SHORT).show()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnTableListFragmentInteractionListener {
        fun onTableListFragmentInteraction(tableSelected: Table?)
    }

    companion object {

        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
                TableListFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }
}
