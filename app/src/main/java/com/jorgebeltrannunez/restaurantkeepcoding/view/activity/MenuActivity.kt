package com.jorgebeltrannunez.restaurantkeepcoding.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Dish
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.DishListFragment
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.EditTextDialogFragment
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.MenuViewModel

import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity(), DishListFragment.OnDishListFragmentInteractionListener, EditTextDialogFragment.EditTextDialogListener {
    private var dishSelected: Dish? = null

    override fun onFinishEditDialog(inputText: String) {
        Log.d("onFinishEditDialog", inputText)
        if (dishSelected != null) {
            viewModel.addDish(dishSelected?.id!!, inputText).subscribe({
                finish()
            })
        }
    }

    private lateinit var viewModel: MenuViewModel
    private val billId by lazy {
        intent.getIntExtra(ARG_BILL_ID, 0)
    }

    override fun onDishListFragmentInteraction(dish: Dish?) {
        dishSelected = dish
        showEditDialog()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        toolbar_menu.title = getString(R.string.menu)
        setSupportActionBar(toolbar_menu)
        viewModel = MenuViewModel(billId)
        setupDishesList()
    }

    private fun setupDishesList() {
        val dishesList = DishListFragment.newInstance(resources.getInteger(R.integer.dishes_list_column), true)
        viewModel.dishesList = dishesList
        supportFragmentManager.beginTransaction()
                .replace(R.id.dishes_list_menu, dishesList)
                .commit()
    }

    private fun showEditDialog() {
        val editNameDialogFragment = EditTextDialogFragment.newInstance(getString(R.string.note), getString(R.string.note_dish))
        editNameDialogFragment.show(supportFragmentManager, "fragment_edit_text")
    }

    private fun showSnackbar(text: String, view: View) {
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show()
    }

    companion object {

        const val ARG_BILL_ID = "ARG_BILL_ID"

        fun intent(context: Context, billId: Int): Intent {
            val intent = Intent(context, MenuActivity::class.java)
            intent.putExtra(ARG_BILL_ID, billId)

            return intent
        }
    }

}
