package com.jorgebeltrannunez.restaurantkeepcoding.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jorgebeltrannunez.restaurantkeepcoding.App
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Bill
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.adapter.BillRecyclerViewAdapter

import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.util.MvvmFragment
import com.jorgebeltrannunez.restaurantkeepcoding.viewmodel.model.BillsList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [BillListFragment.OnBillListFragmentInteractionListener] interface.
 */
class BillListFragment : MvvmFragment(), ListRecycleView {

    private var columnCount = 1

    private lateinit var recyclerView: RecyclerView

    private val tableId: Int by lazy {
        val argTableId = this.arguments?.getInt(ARG_TABLE_ID) ?: run { 0 }
        argTableId
    }
    val billListViewModel = App.injectBillListViewModel()

    private var listenerBill: OnBillListFragmentInteractionListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupArguments()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_bill_list, container, false)
        setupRecyclerView(view)
        return view
    }

    override fun onStart() {
        super.onStart()
        refreshList()
    }

    override fun onResume() {
        super.onResume()
        refreshList()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnBillListFragmentInteractionListener) {
            listenerBill = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnBillListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listenerBill = null
    }

    override fun setupRecyclerView(view: View) {
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                recyclerView = this
                adapter = BillRecyclerViewAdapter(emptyList(), listenerBill)
            }
        }
    }

    override fun setupArguments() {
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
        billListViewModel.tableId = tableId
    }

    override fun refreshList() {
        billListViewModel.getBills()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy({
                    showError()
                }, {
                    Log.d("onStart", "Received UIModel with tables COMPLETE")
                }, {
                    Log.d("onStart", "Received UIModel with tables")
                    showBills(it)
                })
    }

    private fun showBills(billsList: BillsList) {
        if (!billsList.bills.isEmpty()) {
            recyclerView.adapter = BillRecyclerViewAdapter(billsList.bills, listenerBill)
        }
    }

    private fun showError() {
        Toast.makeText(context, getString(R.string.error_occurred), Toast.LENGTH_SHORT).show()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnBillListFragmentInteractionListener {
        fun onBillListFragmentInteraction(billSelected: Bill?)
    }

    companion object {

        const val ARG_COLUMN_COUNT = "column-count"
        const val ARG_TABLE_ID = "ARG_TABLE_ID"

        @JvmStatic
        fun newInstance(columnCount: Int, tableId: Int) =
                BillListFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                        putInt(ARG_TABLE_ID, tableId)
                    }
                }
    }
}
