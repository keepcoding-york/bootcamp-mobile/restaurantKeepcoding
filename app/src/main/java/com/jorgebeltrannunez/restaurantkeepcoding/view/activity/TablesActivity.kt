package com.jorgebeltrannunez.restaurantkeepcoding.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Table
import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.TableListFragment

class TablesActivity : AppCompatActivity(), TableListFragment.OnTableListFragmentInteractionListener {
    override fun onTableListFragmentInteraction(tableSelected: Table?) {
        if (tableSelected != null) {
            Log.v("Listener", "OnTableListFragmentInteractionListener ${tableSelected.id}")
            startActivity(TableActivity.intent(this , tableSelected.id))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tables)
//        if (savedInstanceState == null) {
        val sampleList = TableListFragment.newInstance(resources.getInteger(R.integer.table_list_column))
        supportFragmentManager.beginTransaction()
                .replace(R.id.frag_container, sampleList)
                .commit()
//        }
    }
}
