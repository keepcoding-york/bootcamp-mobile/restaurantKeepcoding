package com.jorgebeltrannunez.restaurantkeepcoding.view.fragment

import android.view.View

interface ListRecycleView {
    fun refreshList()
    fun setupRecyclerView(view: View)
    fun setupArguments()
}