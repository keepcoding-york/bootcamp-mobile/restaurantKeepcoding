package com.jorgebeltrannunez.restaurantkeepcoding.view.fragment

import android.content.Context
import android.support.v4.app.DialogFragment
import android.widget.EditText
import android.os.Bundle
import android.view.*
import android.widget.TextView
import com.jorgebeltrannunez.restaurantkeepcoding.R
import android.view.inputmethod.EditorInfo
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.android.schedulers.AndroidSchedulers
import android.view.Gravity
import android.view.WindowManager
import android.graphics.Point
import android.widget.Button
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxCompoundButton


class EditTextDialogFragment : DialogFragment() {

    private lateinit var editText: EditText
    private lateinit var descriptionDialog: TextView
    private lateinit var acceptButton: Button
    private lateinit var cancelButton: Button
    private lateinit var title: String
    private lateinit var description: String
    private lateinit var text: String
    private var listenerDialog: EditTextDialogListener? = null

    interface EditTextDialogListener {
        fun onFinishEditDialog(inputText: String)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_text, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupArguments()
        setupSubviews(view)
        setupRxBinding()
    }

    private fun setupArguments() {
        arguments?.let {
            title = it.getString(ARG_TITLE)
            description = it.getString(ARG_DESCRIPTION)
        }
    }

    private fun setupSubviews(view: View) {
        editText = view.findViewById(R.id.edit_text_dialog)
        descriptionDialog = view.findViewById(R.id.description_dialog)
        acceptButton = view.findViewById(R.id.accept_dialog)
        cancelButton = view.findViewById(R.id.cancel_dialog)
        dialog.setTitle(title)
        descriptionDialog.text = description
        editText.requestFocus()
        dialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    private fun setupRxBinding() {
        RxTextView.editorActionEvents(editText)
                .filter { it ->
                    it.actionId() == EditorInfo.IME_ACTION_DONE
                }
                .subscribe {
                    onEditorAction()
                }
        RxTextView.afterTextChangeEvents(editText)
//                .debounce(1000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { textEvent ->
                    text = textEvent.view().text.toString()
                }
        RxView.clicks(acceptButton)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onEditorAction()
                })
        RxView.clicks(cancelButton)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismiss()
                })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is EditTextDialogListener) {
            listenerDialog = context
        } else {
            throw RuntimeException(context.toString() + " must implement EditTextDialogListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listenerDialog = null
    }

    override fun onResume() {
        val window = dialog.window
        val size = Point()
        val display = window.windowManager.defaultDisplay
        display.getSize(size)
        window.setLayout((size.x * 0.75).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)
        super.onResume()
    }

    private fun onEditorAction() {
        listenerDialog?.onFinishEditDialog(text)
        dismiss()
    }

    companion object {

        const val ARG_TITLE = "ARG_TITLE"
        const val ARG_DESCRIPTION = "ARG_DESCRIPTION"

        @JvmStatic
        fun newInstance(title: String, description: String) =
                EditTextDialogFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_TITLE, title)
                        putString(ARG_DESCRIPTION, description)
                    }
                }

    }
}