package com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Bill


import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.BillListFragment.OnBillListFragmentInteractionListener

import kotlinx.android.synthetic.main.fragment_bill.view.*

/**
 * [RecyclerView.Adapter] that can display a [Bill] and makes a call to the
 * specified [OnBillListFragmentInteractionListener].
 */
class BillRecyclerViewAdapter(
        private val billList: List<Bill>,
        private val listenerBill: OnBillListFragmentInteractionListener?)
    : RecyclerView.Adapter<BillRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val bill = v.tag as Bill
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an bill has been selected.
            listenerBill?.onBillListFragmentInteraction(bill)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_bill, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bill = billList[position]
        holder.idBill.text = bill.id.toString()
        holder.numberPlatesBill.text = bill.dishes.size.toString()
        holder.totalBill.text = holder.totalBill.context.getString(R.string.type_money,  bill.total.toString(), "€")

        with(holder.mView) {
            tag = bill
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = billList.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val idBill: TextView = mView.idBill
        val numberPlatesBill: TextView = mView.numberPlatesBill
        val totalBill: TextView = mView.totalBill

        override fun toString(): String {
            return super.toString() + " '" + idBill.text + "'"
        }
    }
}
