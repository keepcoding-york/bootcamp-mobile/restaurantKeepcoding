package com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Table


import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.TableListFragment.OnTableListFragmentInteractionListener

import kotlinx.android.synthetic.main.fragment_table.view.*

/**
 * [RecyclerView.Adapter] that can display a [Table] and makes a call to the
 * specified [OnTableListFragmentInteractionListener].
 */
class TableRecyclerViewAdapter(
        private val tableList: List<Table>,
        private val listenerTable: OnTableListFragmentInteractionListener?)
    : RecyclerView.Adapter<TableRecyclerViewAdapter.TableViewHolder>() {

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val table = v.tag as Table
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an table has been selected.
            listenerTable?.onTableListFragmentInteraction(table)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TableViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_table, parent, false)
        return TableViewHolder(view)
    }

    override fun onBindViewHolder(holderTable: TableViewHolder, position: Int) {
        val table = tableList[position]
        holderTable.numberOfTable.text = table.id.toString()

        with(holderTable.mView) {
            tag = table
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount(): Int = tableList.size

    inner class TableViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val numberOfTable: TextView = mView.numberOfTable

        override fun toString(): String {
            return super.toString() + " '" + numberOfTable.text + "'"
        }
    }
}
