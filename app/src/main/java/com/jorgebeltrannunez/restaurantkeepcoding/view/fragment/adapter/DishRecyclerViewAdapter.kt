package com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.jorgebeltrannunez.restaurantkeepcoding.R
import com.jorgebeltrannunez.restaurantkeepcoding.repository.model.Dish
import com.jorgebeltrannunez.restaurantkeepcoding.util.generateLinearGridLayoutAllergens
import com.jorgebeltrannunez.restaurantkeepcoding.util.getImageDish


import com.jorgebeltrannunez.restaurantkeepcoding.view.fragment.DishListFragment.OnDishListFragmentInteractionListener

import kotlinx.android.synthetic.main.fragment_dish.view.*


/**
 * [RecyclerView.Adapter] that can display a [Dish] and makes a call to the
 * specified [OnDishListFragmentInteractionListener].
 */
class DishRecyclerViewAdapter(
        private val dishList: List<Dish>,
        private val listenerDish: OnDishListFragmentInteractionListener?)
    : RecyclerView.Adapter<DishRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val dish = v.tag as Dish
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an dish has been selected.
            listenerDish?.onDishListFragmentInteraction(dish)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_dish, parent, false)
        return ViewHolder(view)
    }

    // TODO type money dynamic
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dish = dishList[position]
        holder.nameDish.text = dish.name
        holder.descriptionDish.text = dish.description
        // TODO implement dish.note.isEmpty()
        if (dish.note != null) {
            holder.noteDish.visibility = View.VISIBLE
            holder.noteDish.text = dish.note
        }
        holder.priceDish.text = holder.priceDish.context.getString(R.string.type_money, dish.price.toString(), "€")
        holder.image.setImageResource(getImageDish(dish.id))
        val context = holder.allergensLinearLayout.context
        // FIXME is necessary remove all views for free memory?
        holder.allergensLinearLayout.removeAllViews()
        generateLinearGridLayoutAllergens(context,holder.allergensLinearLayout,5, dish.allergens)

        with(holder.mView) {
            tag = dish
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = dishList.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val nameDish: TextView = mView.nameDish
        val descriptionDish: TextView = mView.descriptionDish
        val noteDish: TextView = mView.noteDish
        val priceDish: TextView = mView.priceDish
        val allergensLinearLayout: LinearLayout = mView.allergensFrameLayout
        val image: ImageView = mView.imageDish

        override fun toString(): String {
            return super.toString() + " '" + nameDish.text + "'"
        }
    }
}
